package com.smilive.shardingjdbc.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @program: springboot-shardingjdbc
 * @Date: 2020/12/12 14:28
 * @Author: smilive
 * @Description:
 */
@Data
@Accessors(chain = true)
@TableName("course")
public class Course  extends Model<Course> {
    @TableId
    private Long cid;
    private String cname;
    @TableField("user_id")
    private Long userId;
    private String status;

    @Override
    protected Serializable pkVal() {
        return this.cid;
    }
}
