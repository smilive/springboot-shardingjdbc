package com.smilive.shardingjdbc.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: springboot-shardingjdbc
 * @Date: 2020/12/14 9:50
 * @Author: smilive
 * @Description:
 */
@Data
@TableName("t_dict")
public class Dict extends Model<Dict> {

    @TableId("dict_id")
    private Long dictId;
    private String status;
    private String value;

    @Override
    protected Serializable pkVal() {
        return this.dictId;
    }
}
