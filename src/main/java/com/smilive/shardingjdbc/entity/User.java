package com.smilive.shardingjdbc.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: springboot-shardingjdbc
 * @Date: 2020/12/14 9:28
 * @Author: smilive
 * @Description:
 */
@Data
@TableName("t_user")
public class User extends Model<User> {

    @TableId("user_id")
    private Long userId;
    private String username;
    private String status;

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }
}
