package com.smilive.shardingjdbc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.smilive.shardingjdbc.entity.Course;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: springboot-shardingjdbc
 * @Date: 2020/12/12 14:29
 * @Author: smilive
 * @Description:
 */
@Mapper
public interface CourseMapper  extends BaseMapper<Course> {
}
