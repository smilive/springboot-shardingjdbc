package com.smilive.shardingjdbc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.smilive.shardingjdbc.entity.Dict;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: springboot-shardingjdbc
 * @Date: 2020/12/14 9:52
 * @Author: smilive
 * @Description:
 */
@Mapper
public interface DictMapper extends BaseMapper<Dict> {
}
