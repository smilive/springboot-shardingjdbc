package com.smilive.shardingjdbc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.smilive.shardingjdbc.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: springboot-shardingjdbc
 * @Date: 2020/12/14 9:30
 * @Author: smilive
 * @Description:
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
