package com.smilive.shardingjdbc;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.Condition;
import com.smilive.shardingjdbc.entity.Course;
import com.smilive.shardingjdbc.entity.Dict;
import com.smilive.shardingjdbc.entity.User;
import com.smilive.shardingjdbc.mapper.CourseMapper;
import com.smilive.shardingjdbc.mapper.DictMapper;
import com.smilive.shardingjdbc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Random;

@SpringBootTest
class SpringbootShardingjdbcApplicationTests {

    @Autowired
    private CourseMapper courseMapper;

    //***************** 水平分表 *****************

    //添加课程
    @Test
    public void addCourse01() {
        for (int i = 0; i < 9; i++) {
            Course course = new Course();
            //cid由我们设置的策略，雪花算法进行生成
            course.setCname("Java"+i);
            course.setUserId(100L+i);
            course.setStatus("Normal");
            course.insert();
        }

    }

    //查询指定课程
    @Test
    public void findCourse01() {

        System.out.println(courseMapper.selectOne(new Course().setCid(544530178529820672L)));
    }

    //查询所有课程 并按照userId排序
    @Test
    public void findAllCourse01() {
        System.out.println(JSON.toJSONString(courseMapper.selectList(Condition.create().orderBy("user_id"))));
    }

    //***************** 水平分库 *****************

    //添加课程
    @Test
    public void addCourse02() {
        Random random = new Random();
        for (int i = 0; i < 18; i++) {
            Course course = new Course();
            //cid由我们设置的策略，雪花算法进行生成
            course.setCname("Java"+i);
            course.setUserId(Long.valueOf(random.nextInt(10)));
            course.setStatus("Normal");
            course.insert();
        }

    }

    //查询指定课程
    @Test
    public void findCourse02() {

        System.out.println(courseMapper.selectOne(new Course().setCid(544546472415199232L)));
    }

    //修改指定课程
    @Test
    public void updateCourse02() {
        //courseMapper.updateById(new Course().setCid(544546472415199232L).setCname("UPDATE"));

        //更改usreId 看指定库是否跟着改变 --> 否
        courseMapper.updateById(new Course().setCid(544546470938804225L).setUserId(20L).setCname("UPDATE"));

    }

    //删除指定课程
    @Test
    public void deleteCourse02() {
        //删除指定id
        //courseMapper.deleteById(new Course().setCid(544546472415199232L));

        //courseMapper.delete(Condition.create().eq("cname","Java7"));

        //删除指定user_id
        courseMapper.delete(Condition.create().eq("user_id",2));

    }

    //删除所有课程
    @Test
    public void deleteAllCourse02() {
        courseMapper.delete(null);

    }


    //***************** 垂直分库 *****************

    @Autowired
    private UserMapper userMapper;

    @Test
    public void addUser(){
        User user = new User();
        user.setUsername("Jack");
        user.setStatus("Normal");
        user.insert();
    }

    @Test
    public void findUser() {
        userMapper.selectById(1338296977347665922L);
    }


    //***************** 公共表 *****************

    @Autowired
    private DictMapper dictMapper;

    @Test
    public void addDict() {
        Dict dict = new Dict();
        dict.setStatus("Normal");
        dict.setValue("启用");
        dictMapper.insert(dict);
    }

    @Test
    public void deleteDict() {
        dictMapper.deleteById(1338301060749996033L);
    }


    //***************** 读写分离 *****************

    @Test
    public void addUserDB(){
        User user = new User();
        user.setUsername("Jack");
        user.setStatus("Normal");
        userMapper.insert(user);
    }

    @Test
    public void findUserDB() {
        userMapper.selectById(1338301060749996033L);
    }

    @Test
    public void deleteUserDB() {
        userMapper.deleteById(1338374350252371969L);
    }




}
